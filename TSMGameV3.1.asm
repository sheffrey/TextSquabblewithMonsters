.data 

#######Print Functionalities#######
outputRegister:	.word 0xffff000c # This is the display part of the Keyboard MMIO tool. We will also use this register to set the formatting of
outputRegisterIndicator: .word 0xffff0008 
#######InGame Data Strings#########
WelcomeMessage: .asciiz "Welcome to the dungeon. Only thou who defeats the King of Monsters may escape.\n"
NorthDoorMessage: .asciiz "There is a door to the north.\n"
SouthDoorMessage: .asciiz "There is a door to the south.\n"
WestDoorMessage: .asciiz "There is a door to the west.\n"
EastDoorMessage: .asciiz "There is a door to the east.\n"
FriendlyMessage: .asciiz "There is a friendly character in the room.\n"
SwordMessage: .asciiz "There is an attack power up in the room.\n"
SwordPickup: .asciiz "You picked up an attack power up.\n"
HealthMessage: .asciiz "There is a health power up in the room.\n"
HealthPickup: .asciiz"You picked up a health power up.\n"
DamageMessage: .asciiz "You weren't strong enough! You took damage!\n"
ExperienceMessage: .asciiz "Defeating the monster bolsters your attack power!\n"
MoveNorth: .asciiz "W: Move North\n"
MoveSouth: .asciiz "S: Move South\n"
MoveWest: .asciiz "A: Move West\n"
MoveEast: .asciiz "D: Move East\n"
Interact: .asciiz "Space: Interact\n"
MidBoss: .asciiz "A powerful monster is in this room. He is guarding the stairs to the next floor!\n"
MidBossDefeat: .asciiz "You defeated the Boss and descended the stairs to the next floor.\n"
FinalBoss: .asciiz "The King of Monsters stands before you. He looks extremely dangerous! Beware!\n "
FinalBossDefeat: .asciiz "You have defeated the King of Monsters! Congratulation!\n"
EasyMonster: .asciiz "There is a weak enemy that resides in this room.\n"
MediumMonster: .asciiz "There is a threatening enemy that resides in this room.\n"
HardMonster: .asciiz "There is a powerful enemy that resides in this room.\n"
SecretRoom: .asciiz "You found the secret room!\n"
###################################

screen: .word 0x10000000 #stores bitmap pixel location
roomsize:.word 28 #used to loop draw functions
key:	.word 0xffff0004 #stores keyboard input
Monsters: .space 216
Hearts: .space 216
Swords: .space 216	
.text
GameStart:
	jal	Wipe #Blacks out the screen when the game starts
	j	MenuLoop
MenuLoop:
	lw	$t3, key #Displays a menu until space is pressed
	lw	$t3, ($t3)
	beq	$t3, 0x00000020, Initilize
	j	Menu
Wipe:
	li	$t2, 0x00000000
	lw	$t0, screen
	j	WipeLoop
WipeLoop:
	sw	$t2, ($t0) #Blackens the entire screen
	addi	$t0, $t0, 4
	ble	$t0, 0x10008000, WipeLoop
	jr	$ra	
Initilize:
	jal	Wipe
	jal	DrawUI	#Initilizes UI
	jal	Clear
	li	$s0, 1
	li	$s1, 0
	lw	$t4, roomsize 
	li	$s5, 10	
	li	$s6, 3
	li	$s7, 0
	li	$t7, 0
	j	Interactables
Interactables:
	sw	$s0, Monsters($s1)
	sw	$s0, Hearts($s1)
	sw	$s0, Swords($s1)
	addi	$s1, $s1, 4
	sub	$t4, $t4, 1
	bgt	$t4, -188, Interactables	#Loops until array stores a value for every room
	j	conditions

conditions:	#resets values and branches based off key input
	lw	$t3, key
	lw	$t3, ($t3)
	beq	$t3, 0x00000077, condw #branches based off input key value
	beq	$t3, 0x00000064, condd
	beq	$t3, 0x00000073, conds
	beq	$t3, 0x00000061, conda
	beq	$t3, 0x00000020, condspace
	beq	$t3, 0x00000079, condy
	beq	$t3, 0x0000006E, condn
	beq	$t7, 100, RetryLoop	#impossible condition to loop the game over
	beq	$s6, 0, GameOver	#Jumps to loop when lives hit zero
	j	UpdateUI	#Updates the stats when no button is pressed
	
condw: #updates t6 register for upwards movement
	li	$t6, 1
	j 	endif
condd: #updates t6 register for right movement
	li	$t6, 2 	
	j 	endif
conds: #updates t6 register for downwards movement
	li	$t6, 3
	j 	endif
conda: #updates t6 register for left movement
	li	$t6, 4
	j 	endif
condspace:
	li	$t6, 5
	j	endif
condy:
	li	$t6, 6
	j 	endif
condn:
	li	$t6, 7
	j 	endif

endif: #branches based off t7 register to current room branch conditions for movement
	beq	$t7, 1, Room1
	beq	$t7, 2, Room2
	beq	$t7, 3, Room3
	beq	$t7, 4, Room4
	beq	$t7, 5, Room5
	beq	$t7, 6, Room6
	beq	$t7, 7, Room7
	beq	$t7, 8, Room8
	beq	$t7, 9, Room9
	beq	$t7, 10, Room10
	beq	$t7, 11, Room11
	beq	$t7, 12, Room12
	beq	$t7, 13, Room13
	beq	$t7, 14, Room14
	beq	$t7, 15, Room15
	beq	$t7, 16, Room16
	beq	$t7, 17, Room17
	beq	$t7, 18, Room18
	beq	$t7, 19, Room19
	beq	$t7, 20, Room20
	beq	$t7, 21, Room21
	beq	$t7, 22, Room22
	beq	$t7, 23, Room23
	beq	$t7, 24, Room24
	beq	$t7, 25, Room25
	beq	$t7, 26, Room26
	beq	$t7, 27, Room27
	beq	$t7, 28, Room28
	beq	$t7, 29, Room29
	beq	$t7, 30, Room30
	beq	$t7, 31, Room31
	beq	$t7, 32, Room32
	beq	$t7, 33, Room33
	beq	$t7, 34, Room34
	beq	$t7, 35, Room35
	beq	$t7, 36, Room36
	beq	$t7, 37, Room37
	beq	$t7, 38, Room38
	beq	$t7, 39, Room39
	beq	$t7, 40, Room40
	beq	$t7, 41, Room41
	beq	$t7, 42, Room42
	beq	$t7, 43, Room43
	beq	$t7, 44, Room44
	beq	$t7, 45, Room45
	beq	$t7, 46, Room46
	beq	$t7, 47, Room47
	beq	$t7, 48, Room48
	beq	$t7, 49, Room49
	beq	$t7, 50, Room50
	beq	$t7, 51, Room51
	beq	$t7, 52, Room52
	beq	$t7, 53, Room53
	beq	$t7, 54, Room54
	beq	$t7, 100, RetryLoop

RoomStart:
	lw	$t4, roomsize
	jal	TopWall #draw four blank walls to fom initial room
	jal	LeftWall
	jal	RightWall
	jal 	BottomWall
	j	Draw23
	
Draw1: #draw Room1
	li	$t7, 1
	jal	Clear
	jal	ClearText
	jal	TopWall #draws specific room characteristics. ie: doors, monsters, etc
	jal	LeftWall	
	jal	RightWall
	jal	BottomDoor
	#print text
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	
	#draw sword
	beq	$t9, 1, Sword
	j 	Room1
Room1: #branch conditions for Room1
	beq	$t6, 3, Draw6 #branches to connecting rooms based on player movement
	beq	$t6, 5, AttackUp
	j	conditions #loop for condition check

Draw2: #draw Room2, for detailed commenting see Draw1 & Room1
	li	$t7, 2
	jal 	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall	
	jal	RightWall
	jal	BottomDoor
	
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	
	beq	$t8, 1, Heart	
	j 	Room2
Room2: #branch conditions for Room2
	beq	$t6, 3, Draw8
	beq	$t6, 5, HeartUp
	j	conditions
	
Draw3: #draw Room3, for detailed commenting see Draw1 & Room1
	li	$t7, 3
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall	
	jal	RightDoor
	jal	BottomWall
	#print text
	la	$a0, MidBoss
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveEast
	jal	printString
	
	jal	Clear
	beq	$t1, 1, DrawFirstBoss
	j 	Room3
Room3: #branch conditions for Room3
	li	$t5, 6
	beq	$t6, 2, Draw4
	beq	$t6, 5, KillBoss
	j	conditions
	
Draw4: #draw Room4, for detailed commenting see Draw1 & Room1
	li	$t7, 4
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomDoor
		#print text
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, MoveWest
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	j 	Room4	
Room4: #branch conditions for Room4
	beq	$t6, 4, Draw3
	beq	$t6, 3, Draw10
	j	conditions
	
Draw5: 
	li	$t7, 5
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightWall
	jal	BottomDoor
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	
	j 	Room5
Room5: 
	beq	$t6, 3, Draw11
	j	conditions
	
Draw6: 
	li	$t7, 6
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
		#print text
	la	$a0, MediumMonster
	jal	printString
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy

	j 	Room6
Room6: 
	li	$t5, 3
	beq	$t6, 1, Draw1
	beq	$t6, 2, Draw7
	beq	$t6, 3, Draw13
	beq	$t6, 5, KillEnemy
	j	conditions	
	
Draw7: 
	li	$t7, 7
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomDoor
		#print text
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, MoveWest
	jal	printString
	la	$a0, MoveSouth
	jal	printString

	j 	Room7
Room7: 
	beq	$t6, 4, Draw6
	beq	$t6, 3, Draw14
	j	conditions	
	
Draw8: 
	li	$t7, 8
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	#print text
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString

	j 	Room8
Room8:
	beq	$t6, 1, Draw2
	beq	$t6, 2, Draw9
	beq	$t6, 3, Draw16
	j	conditions
	
Draw9: 
	li	$t7, 9
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomDoor
	#print text
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, MoveWest
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	
	j 	Room9
Room9:
	beq	$t6, 4, Draw8
	beq	$t6, 3, Draw17
	j	conditions	
	
Draw10: 
	li	$t7, 10
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	#print text
	la	$a0, MediumMonster
	jal	printString
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy

	j 	Room10
Room10:
	li	$t5, 3
	beq	$t6, 1, Draw4
	beq	$t6, 2, Draw11
	beq	$t6, 3, Draw18
	beq	$t6, 5, KillEnemy
	j	conditions
	
Draw11: 
	li	$t7, 11
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomDoor
	la	$a0, MediumMonster
	jal	printString
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la 	$a0, MoveWest
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString

	j 	Room11
Room11:
	beq	$t6, 1, Draw5
	beq	$t6, 2, Draw12
	beq	$t6, 3, Draw19
	beq 	$t6, 4, Draw10
	j	conditions	
	
Draw12: 
	li	$t7, 12
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveWest
	jal	printString
	beq	$t9, 1, Sword
	
	j 	Room12
Room12:
	beq 	$t6, 4, Draw11
	beq	$t6, 5, AttackUp
	j	conditions	
	
Draw13: 
	li	$t7, 13
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	#print text
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString

	j 	Room13
Room13:
	beq	$t6, 1, Draw6
	beq	$t6, 2, Draw14
	beq	$t6, 3, Draw20
	j	conditions	
	
Draw14: 
	li	$t7, 14
	jal	Clear
	jal	ClearText	
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomWall
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la 	$a0, MoveWest
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	j 	Room14
Room14:
	beq	$t6, 1, Draw7
	beq	$t6, 2, Draw15
	beq 	$t6, 4, Draw13
	j	conditions	
	
Draw15: 
	li	$t7, 15
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightDoor
	jal	BottomDoor
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la 	$a0, MoveWest
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString	

	j 	Room15
Room15:
	beq	$t6, 2, Draw16
	beq	$t6, 3, Draw21
	beq 	$t6, 4, Draw14
	j	conditions					
					
Draw16: 
	li	$t7, 16
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomDoor
	#print text
	la	$a0, EasyMonster
	jal	printString
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la 	$a0, MoveWest
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString	

	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room16
Room16:
	li	$t5, 1
	beq	$t6, 1, Draw8
	beq	$t6, 2, Draw17
	beq	$t6, 3, Draw22
	beq 	$t6, 4, Draw15
	beq	$t6, 5, KillEnemy
	j	conditions																																						

Draw17: 
	li	$t7, 17
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomDoor
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la 	$a0, MoveWest
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	la	$a0, MoveSouth
	jal	printString
	
	beq	$t9, 1, Sword
	j 	Room17
Room17:
	beq	$t6, 1, Draw9
	beq	$t6, 2, Draw18
	beq	$t6, 3, Draw23
	beq 	$t6, 4, Draw16
	beq	$t6, 5, AttackUp
	j	conditions
	
Draw18: 
	li	$t7, 18
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, MoveNorth
	jal	printString
	la	$a0, MoveWest
	jal	printString
	j 	Room18
Room18:
	beq	$t6, 1, Draw10
	beq 	$t6, 4, Draw17
	j	conditions	
	
Draw19: 
	li	$t7, 19
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightWall
	jal	BottomWall
	#print text
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, MoveNorth
	jal	printString
	j 	Room19
Room19:
	beq	$t6, 1, Draw11
	beq	$t6, 3, Draw24
	j	conditions	
	
Draw20: 
	li	$t7, 20
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightWall
	jal	BottomWall
	#Text print
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, MoveNorth
	jal	printString
	beq	$t9, 1, Sword
	j 	Room20
Room20:
	beq	$t6, 1, Draw13
	beq	$t6, 5, AttackUp
	j	conditions	
	
Draw21: 
	li	$t7, 21
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomWall
	#Text print
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, MoveNorth
	jal	printString
	la	$a0, MoveEast
	jal	printString
	j 	Room21
Room21:
	beq	$t6, 1, Draw15
	beq	$t6, 2, Draw22
	j	conditions	
	
Draw22: 
	li	$t7, 22
	jal	Clear
	jal	ClearText
	#draw room
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomWall
	#Text print
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la 	$a0, MoveNorth
	jal	printString #Print string
	la 	$a0, MoveWest
	jal	printString #Print string
	la	$a0, MoveEast
	jal	printString #print string
	#draw heart
	beq	$t8, 1, Heart
	j 	Room22
Room22:
	beq	$t6, 1, Draw16
	beq	$t6, 2, Draw23
	beq 	$t6, 4, Draw21
	beq	$t6, 5, HeartUp
	j	conditions	
	
Draw23: 
	li	$t7, 23
	jal	Clear
	jal	ClearText
	#draw room
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	#print text
	la	$a0, WelcomeMessage #Load string
	jal	printString #Print string
	la 	$a0, NorthDoorMessage
	jal	printString #Print string
	la 	$a0, WestDoorMessage
	jal	printString #Print string
	la 	$a0, MoveNorth
	jal	printString #Print string
	la 	$a0, MoveWest
	jal	printString #Print string
	j 	Room23
Room23:
	beq	$t6, 1, Draw17
	beq 	$t6, 4, Draw22
	j	conditions

Draw24: 
	li	$t7, 24
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightWall
	jal	BottomWall
	#printtext
	la	$a0, SecretRoom
	jal	printString
	la	$a0, NorthDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveNorth
	jal	printString
	#drawItem
	beq	$t9, 1, Sword
	
	j 	Room24
Room24:
	beq	$t6, 1, Draw19
	beq	$t6, 5, AttackUp
	j	conditions
	
Draw25: 
	li	$t7, 25
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightDoor
	jal	BottomWall
	
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveEast
	jal	printString
	
	beq	$t8, 1, Heart
	j 	Room25
Room25:
	beq	$t6, 2, Draw26
	beq	$t6, 5, HeartUp
	j	conditions	
	
Draw26: 
	li	$t7, 26
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomDoor
	
	la	$a0, MediumMonster
	jal	printString
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, WestDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	la	$a0, MoveWest
	jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room26
Room26:
	li	$t5, 11
	beq	$t6, 3, Draw28
	beq 	$t6, 4, Draw25
	beq	$t6, 5, KillEnemy
	j	conditions		
	
Draw27: 
	li	$t7, 27
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightWall
	jal	BottomDoor
	
	la	$a0, SouthDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveSouth
	jal	printString
	
	beq	$t9, 1, Sword
	j 	Room27
Room27:
	beq	$t6, 3, Draw32
	beq	$t6, 5, AttackUp
	j	conditions			
			
Draw28: 
	li	$t7, 28
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightWall
	jal	BottomDoor
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal	printString
	
	j 	Room28
Room28:
	beq	$t6, 1, Draw26
	beq	$t6, 3, Draw33
	j	conditions				
					
Draw29: 
	li	$t7, 29
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	
	la	$a0, HardMonster
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveSouth
        jal	printString
        la	$a0, MoveEast
        jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room29
Room29:
	li	$t5, 13
	beq	$t6, 2, Draw30
	beq	$t6, 3, Draw35
	beq	$t6, 5, KillEnemy
	j	conditions						
				
Draw30: 
	li	$t7, 30
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	
	la	$a0, FinalBoss
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveWest
        jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawFinalBoss
	j 	Room30
Room30:
	li	$t5, 15
	beq 	$t6, 4, Draw29
	#########
	beq	$t6, 5, KillFinalBoss
	#########
	j	conditions	
	
Draw31: 
	li	$t7, 31
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room31
Room31:
	beq	$t6, 2, Draw32
	beq	$t6, 3, Draw37
	j	conditions	
	
Draw32: 
	li	$t7, 32
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomDoor
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
	
	j 	Room32
Room32:
	beq	$t6, 1, Draw27
	beq	$t6, 3, Draw38
	beq 	$t6, 4, Draw31
	j	conditions
	
Draw33: 
	li	$t7, 33
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room33
Room33:
	beq	$t6, 1, Draw28
	beq	$t6, 2, Draw34
	beq	$t6, 3, Draw39
	j	conditions
	
Draw34: 
	li	$t7, 34
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightDoor
	jal	BottomWall
	
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveWest
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room34
Room34:
	beq	$t6, 2, Draw35
	beq 	$t6, 4, Draw33
	j	conditions	
	
Draw35: 
	li	$t7, 35
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
	
	j 	Room35
Room35:
	beq	$t6, 1, Draw29
	beq 	$t6, 4, Draw34
	j	conditions
	
Draw36: 
	li	$t7, 36
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightWall
	jal	BottomDoor
	
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, MoveSouth
        jal 	printString
	
	j 	Room36
Room36:
	beq	$t6, 3, Draw42
	j	conditions	
	
Draw37: 
	li	$t7, 37
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightWall
	jal	BottomDoor
	
	la	$a0, EasyMonster
	jal	printString
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room37
Room37:
	li	$t5, 8
	beq	$t6, 1, Draw31
	beq	$t6, 3, Draw43
	beq	$t6, 5, KillEnemy
	j	conditions

Draw38: 
	li	$t7, 38
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomWall
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room38
Room38:
	beq	$t6, 1, Draw32
	beq	$t6, 2, Draw39
	j	conditions	
	
Draw39: 
	li	$t7, 39
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomDoor
	
	la	$a0, MediumMonster
        jal	printString
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room39
Room39:
	li	$t5, 11
	beq	$t6, 1, Draw33
	beq	$t6, 2, Draw40
	beq	$t6, 3, Draw45
	beq 	$t6, 4, Draw38
	beq	$t6, 5, KillEnemy
	j	conditions
	
Draw40: 
	li	$t7, 40
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveWest
        jal 	printString
	
	beq	$t9, 1, Sword
	j 	Room40
Room40:
	beq 	$t6, 4, Draw39
	beq	$t6, 5, AttackUp
	j	conditions

Draw41: 
	li	$t7, 41
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightDoor
	jal	BottomDoor
	
	la	$a0, EasyMonster
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room41
Room41:
	li	$t5, 8
	beq	$t6, 2, Draw42
	beq	$t6, 3, Draw47
	beq	$t6, 5, KillEnemy
	j	conditions
	
Draw42: 
	li	$t7, 42
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomDoor
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
	
	j 	Room42
Room42:
	beq	$t6, 1, Draw36
	beq	$t6, 3, Draw48
	beq 	$t6, 4, Draw41
	j	conditions
	
Draw43: 
	li	$t7, 43
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightDoor
	jal	BottomWall
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room43
Room43:
	beq	$t6, 1, Draw37
	beq	$t6, 2, Draw44
	j	conditions	
	
Draw44: 
	li	$t7, 44
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightDoor
	jal	BottomWall
	
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveWest
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room44
Room44:
	beq	$t6, 2, Draw45
	beq 	$t6, 4, Draw43
	j	conditions
	
Draw45: 
	li	$t7, 45
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightDoor
	jal	BottomDoor
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room45
Room45:
	beq	$t6, 1, Draw39
	beq	$t6, 2, Draw46
	beq	$t6, 3, Draw50
	beq 	$t6, 4, Draw44
	j	conditions
	
Draw46: 
	li	$t7, 46
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightDoor
	jal	BottomWall
	
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveWest
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room46
Room46:
	beq	$t6, 2, Draw47
	beq 	$t6, 4, Draw45
	j	conditions	
	
Draw47: 
	li	$t7, 47
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
	
	j 	Room47
Room47:
	beq	$t6, 1, Draw41
	beq 	$t6, 4, Draw46
	j	conditions	
	
Draw48: 
	li	$t7, 48
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftWall
	jal	RightWall
	jal	BottomDoor
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, SouthDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveSouth
        jal 	printString
	
	j 	Room48
Room48:
	beq	$t6, 1, Draw42
	beq	$t6, 3, Draw53
	j	conditions
	
Draw49: 
	li	$t7, 49
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightDoor
	jal	BottomWall
	
	la	$a0, EastDoorMessage
	jal	printString
	la	$a0, Interact
	jal	printString
	la	$a0, MoveEast
	jal	printString
	
	beq	$t8, 1, Heart
	j 	Room49
Room49:
	beq	$t6, 2, Draw50
	beq	$t6, 5, HeartUp
	j	conditions
	
Draw50: 
	li	$t7, 50
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
	
	j 	Room50
Room50:
	beq	$t6, 1, Draw45
	beq 	$t6, 4, Draw49
	j	conditions
	
Draw51: 
	li	$t7, 51
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightDoor
	jal	BottomWall
	
	la	$a0, MediumMonster
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveEast
        jal 	printString
	
	jal	Clear
	beq	$t1, 1, DrawEnemy
	j 	Room51
Room51:
	li	$t5, 11
	beq	$t6, 2, Draw52
	beq	$t6, 3, Draw54
	beq	$t6, 5, KillEnemy
	j	conditions	
	
Draw52: 
	li	$t7, 52
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftDoor
	jal	RightDoor
	jal	BottomWall
	
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, EastDoorMessage
        jal	printString
        la	$a0, MoveWest
        jal 	printString
        la	$a0, MoveEast
        jal 	printString
	
	j 	Room52
Room52:
	beq	$t6, 2, Draw53
	beq 	$t6, 4, Draw51
	j	conditions
	
Draw53: 
	li	$t7, 53
	jal	Clear
	jal	ClearText
	jal	TopDoor
	jal	LeftDoor
	jal	RightWall
	jal	BottomWall
	
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, WestDoorMessage
        jal	printString
        la	$a0, Interact
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
        la	$a0, MoveWest
        jal 	printString
	
	beq	$t9, 1, Sword
	j 	Room53
Room53:
	beq	$t6, 1, Draw48
	beq 	$t6, 4, Draw52
	beq	$t6, 5, AttackUp
	j	conditions	
	
Draw54: 
	li	$t7, 54
	jal	Clear
	jal	ClearText
	jal	TopWall
	jal	LeftWall
	jal	RightWall
	jal	BottomWall
	
	la	$a0, SecretRoom
        jal	printString
        la	$a0, NorthDoorMessage
        jal	printString
        la	$a0, MoveNorth
        jal 	printString
	
	beq	$t9, 1, Sword
	j 	Room54
Room54:
	beq	$t6, 1, Draw51
	beq	$t6, 5, AttackUp
	j	conditions								
	
################################################################################################################################
TopWall:	#sets starting location of top wall, calls DrawTBWall
	lw	$t0, screen
	addi	$t0, $t0, 524
	beq	$t4, 28, DrawTBWall
BottomWall:	#sets starting location of bottom wall, calls DrawTBWall
	lw	$t0, screen
	addi	$t0, $t0, 7436
	beq	$t4, 28, DrawTBWall
	
DrawTBWall:	#draws a horizontal wall
	li	$t2, 0x00ffffff	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 1
	bgt	$t4, 0, DrawTBWall #loops until wall is complete
	lw	$t4, roomsize
	jr	$ra #returns to room function to continue drawing and branch
	
LeftWall:	#sets starting location of left wall, calls DrawLRWall
	lw	$t0, screen
	addi	$t0, $t0, 520
	beq	$t4, 28, DrawLRWall
RightWall:	#sets starting location of right, calls DrawLRWall 
	lw	$t0, screen
	addi	$t0, $t0, 632
	beq	$t4, 28, DrawLRWall
	
DrawLRWall: 	#draw a vertical wall
	li	$t2, 0x00ffffff #sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sub	$t4, $t4, 1
	bgtz	$t4, DrawLRWall #loops until wall is complete
	lw	$t4, roomsize
	jr	$ra #returns to room function to continue drawing and branch

TopDoor:	#sets starting location of top door, calls DrawTBDoor
	lw	$t0, screen
	addi	$t0, $t0, 560
	beq	$t4, 28, DrawTBDoor
BottomDoor:	#sets starting location of bottom door, calls DrawTBDoor
	lw	$t0, screen
	addi	$t0, $t0, 7472
	beq	$t4, 16, DrawTBDoor
DrawTBDoor:	#draw a horizontal door
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 3
	bgt	$t4, 1, DrawTBDoor
	lw	$t4, roomsize
	jr	$ra
	
RightDoor:	#sets starting location of right door, calls DrawLRDoor
	lw	$t0, screen
	addi	$t0, $t0, 3192
	beq	$t4, 28, DrawLRDoor
LeftDoor:	#sets starting location of left door, calls DrawLRDoor
	lw	$t0, screen
	addi	$t0, $t0, 3080
	beq	$t4, 28, DrawLRDoor
DrawLRDoor:	#draw a door on a vertical door
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sub	$t4, $t4, 3
	bgt	$t4, 4, DrawLRDoor
	lw	$t4, roomsize
	jr	$ra
	
Clear:
	lw	$t3, key
	sw	$zero, ($t3)
	li	$t6, 0
	lw	$t0, screen
	li	$t2, 0x00000000
	lw	$t4, roomsize
	addi	$t0, $t0, 780
	j	ClearLoop
ClearLoop:
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	blt 	$t0, 0x10001D0C, ClearLoop
	sub	$t0, $t0, 6652
	j	ClearLoop2
ClearLoop2:
	sub	$t4, $t4, 1
	bgt	$t4, 1, ClearLoop
	lw	$t4, roomsize
	j	ResetArray
ResetArray:
	sub	$t5, $t7, 1	#Updates registers with Array values
	mul	$s0, $t5, 4
	lw	$t1, Monsters($s0)
	lw	$t8, Hearts($s0)
	lw	$t9, Swords($s0)
	jr	$ra
ClearText:
	lw      $t1, outputRegister #These two lines are what we use to clear the display
	sw	$t1, ($t1)	       #so it doesn't get cluttered.
	jr	$ra

###############################################################################################################
KillBoss:
	bge	$s7, $t5, BossSuccess
	j	BossFailure
BossSuccess:
	jal	Clear
	add	$s7, $s7, $t1
	sw	$zero, Monsters($s0)
	la	$a0, MidBossDefeat
	jal	printString
	j	Draw36
BossFailure:
	jal	Clear
	sub	$s6, $s6, 1
	la	$a0, DamageMessage
	jal	printString
	j	DrawFirstBoss

###
KillFinalBoss:
	bge	$s7, $t5, GameComplete
	j	FinalFailure
FinalFailure:
	jal	Clear
	sub	$s6, $s6, 1
	la	$a0, DamageMessage
	jal	printString
	j	DrawFinalBoss

###
KillEnemy:
	bge	$s7, $t5, KillSuccess
	j	KillFailure
	
KillSuccess:
	jal	Clear
	add	$s7, $s7, $t1
	sw	$zero, Monsters($s0)
	la	$a0, ExperienceMessage
	jal	printString
	j	conditions
KillFailure:
	jal	Clear
	sub	$s6, $s6, 1
	la	$a0, DamageMessage
	jal	printString
	j	DrawEnemy

HeartUp:
	jal	Clear
	add	$s6, $s6, $t8
	sw	$zero, Hearts($s0)
	la	$a0, HealthPickup
	jal	printString
	j	conditions
AttackUp:
	jal	Clear
	add	$s7, $s7, $t9
	sw	$zero, Swords($s0)
	la	$a0, SwordPickup
	jal	printString
	j	conditions
##########################################################################
UpdateUI:
	div	$s6, $s5
	jal	HeartCount1
	jal	HeartCount2
	div	$s7, $s5
	jal	SwordCount1
	jal	SwordCount2
	j	endif
	
HeartCount1: ### Health first digit ### Coordinates are already set
	lw	$t0, screen
	addi	$t0, $t0, 3780
	mflo 	$s4
	beq	$s4, 0, Number0
	beq	$s4, 1, Number1
	beq	$s4, 2, Number2
	beq	$s4, 3, Number3
	beq	$s4, 4, Number4
	beq	$s4, 5, Number5
	beq	$s4, 6, Number6
	beq	$s4, 7, Number7
	beq	$s4, 8, Number8
	beq	$s4, 9, Number9
	jr	$ra
HeartCount2: ### Health second digit ### Coordinates are already set
	lw	$t0, screen
	addi	$t0, $t0, 3796
	mfhi 	$s4
	beq	$s4, 0, Number0
	beq	$s4, 1, Number1
	beq	$s4, 2, Number2
	beq	$s4, 3, Number3
	beq	$s4, 4, Number4
	beq	$s4, 5, Number5
	beq	$s4, 6, Number6
	beq	$s4, 7, Number7
	beq	$s4, 8, Number8
	beq	$s4, 9, Number9
	jr	$ra
SwordCount1: # Attack first digit ### Coordinates are already set
	lw	$t0, screen
	addi	$t0, $t0, 5828
	mflo 	$s4
	beq	$s4, 0, Number0
	beq	$s4, 1, Number1
	beq	$s4, 2, Number2
	beq	$s4, 3, Number3
	beq	$s4, 4, Number4
	beq	$s4, 5, Number5
	beq	$s4, 6, Number6
	beq	$s4, 7, Number7
	beq	$s4, 8, Number8
	beq	$s4, 9, Number9
	jr	$ra
	
SwordCount2:	# Attack second digit ### Coordinates are already set
	lw	$t0, screen
	addi	$t0, $t0, 5844
	mfhi 	$s4
	beq	$s4, 0, Number0
	beq	$s4, 1, Number1
	beq	$s4, 2, Number2
	beq	$s4, 3, Number3
	beq	$s4, 4, Number4
	beq	$s4, 5, Number5
	beq	$s4, 6, Number6
	beq	$s4, 7, Number7
	beq	$s4, 8, Number8
	beq	$s4, 9, Number9
	jr	$ra
#########################################################################################################################

Heart:
	la	$a0, HealthMessage
	jal	printString
	lw	$t0, screen
	li	$t2, 0x00ff0000
	lw	$t4, roomsize
	addi	$t0, $t0, 3128
	j	DrawHeart1
DrawHeart1:
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sub	$t4, $t4, 5
	bgt	$t4, 0, DrawHeart1
	lw	$t4, roomsize
	sub	$t0, $t0, 1032
	j	DrawHeart2
DrawHeart2:
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	subi	$t0, $t0, 252
	sub	$t4, $t4, 3
	bgt	$t4, 1, DrawHeart2
	lw	$t4, roomsize
	sub	$t0, $t0, 288
	j	DrawHeart3
DrawHeart3:
	sw	$t2, ($t0)
	addi	$t0, $t0, 768
	sw	$t2, ($t0)
	subi	$t0, $t0, 756
	sub	$t4, $t4, 9
	bgt	$t4, 1, DrawHeart3
	addi	$t0, $t0, 1000
	j	DrawHeart4
DrawHeart4:
	sw	$t2, ($t0)
	addi	$t0, $t0, 252
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 252
	sw	$t2, ($t0)
	lw 	$t4, roomsize
	j	conditions
	
Sword:

	la	$a0, SwordMessage
	jal	printString
	lw	$t0, screen
	lw	$t4, roomsize
	addi 	$t0, $t0, 2644
	j 	DrawSword1	
DrawSword1: #sword line 1
	li	$t2, 0x00787272 #gray color
	sw	$t2, ($t0)
	addi	$t0, $t0, 252	#diagonal line 
	sub	$t4, $t4, 4	#6 bits long
	bgt	$t4, 4, DrawSword1
	lw	$t4, roomsize
	subi    $t0, $t0, 1516	#for next loop
	j	DrawSword2
DrawSword2: #sword line 2
	sw	$t2, ($t0)
	addi	$t0, $t0, 252
	sub	$t4, $t4, 4
	bgt	$t4, 4, DrawSword2
	lw	$t4, roomsize
	subi    $t0, $t0, 1252
	lw	$t4, roomsize
	j	DrawSword3
DrawSword3: #sword line 2
	sw	$t2, ($t0)
	addi	$t0, $t0, 252
	sub	$t4, $t4, 4
	bgt	$t4, 4, DrawSword3
	lw	$t4, roomsize
	subi	$t0, $t0, 772
	j	DrawHilt1
DrawHilt1: #hilt vertical
	li	$t2, 0x004e2e1e
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sub	$t4, $t4, 5
	bgt	$t4, 4, DrawHilt1
	lw	$t4, roomsize
	subi	$t0, $t0, 516
	lw	$t4, roomsize
DrawHilt2: #hilt horizontal
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 5	
	bgt	$t4, 4, DrawHilt2 
	subi	$t0, $t0, 268	#Draw Literal points
	sw	$t2, ($t0)
	addi	$t0, $t0, 504
	sw	$t2, ($t0)
	lw 	$t4, roomsize
	j	conditions
	
DrawUI: 
	li	$t2, 0x00ffffff
	lw 	$t0, screen
	addi 	$t0, $t0, 1164
	j	DrawS
	
	
DrawS:
	sw	$t2, ($t0)
	addi 	$t9, $zero, 4 #t9 is temp var for loop
	j	SLoop1
SLoop1:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, SLoop1
	
	addi	$t0, $t0, 240	
	sw	$t2, ($t0)
	
	addi	$t0, $t0, 252	
	addi 	$t9, $zero, 5 #t9 is temp var for loop
	j	SLoop2
SLoop2:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, SLoop2
			
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
				
	addi	$t0, $t0, 236	
	addi 	$t9, $zero, 5 #t9 is temp var for loop
	j	SLoop3
SLoop3:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, SLoop3
	
#####	
	lw 	$t0, screen
	addi 	$t0, $t0, 1184
	j	DrawT

DrawT:	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi 	$t9, $zero, 2 #t9 is temp var for loop
TLoop1:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, TLoop1
	
	addi	$t0, $t0, 252	
	sw	$t2, ($t0)
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
	
#####	
	lw 	$t0, screen
	addi 	$t0, $t0, 1200
	j	DrawA
	
DrawA:	
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	addi	$t0, $t0, 248	
	sw	$t2, ($t0)
	addi	$t0, $t0, 12	
	sw	$t2, ($t0)
	addi	$t0, $t0, 244	
	sw	$t2, ($t0)
	addi	$t0, $t0, 4	
	sw	$t2, ($t0)
	addi	$t0, $t0, 4	
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 244	
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)

####

	lw 	$t0, screen
	addi 	$t0, $t0, 1220
	j	DrawT2
	
DrawT2:	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi 	$t9, $zero, 2 #t9 is temp var for loop
	j	TLoop12
TLoop12:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, TLoop12
	
	addi	$t0, $t0, 252	
	sw	$t2, ($t0)
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)	
#####	
	
	lw 	$t0, screen
	addi 	$t0, $t0, 1236
	j	DrawS2
	
DrawS2:
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi 	$t9, $zero, 4 #t9 is temp var for loop
	j	SLoop12
SLoop12:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, SLoop12
	
	addi	$t0, $t0, 240	
	sw	$t2, ($t0)
	
	addi	$t0, $t0, 252	
	addi 	$t9, $zero, 5 #t9 is temp var for loop
	j	SLoop22
	
SLoop22:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, SLoop22
			
	addi	$t0, $t0, 256	
	sw	$t2, ($t0)
				
	addi	$t0, $t0, 236	
	addi 	$t9, $zero, 5 #t9 is temp var for loop
	j	SLoop32
	
SLoop32:	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	subi	$t9, $t9, 1
	bne 	$zero, $t9, SLoop32
	j	HeartIcon

HeartIcon:
	li	$t2, 0x00ff0000
	lw 	$t0, screen
	addi	$t0, $t0, 3488
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	lw 	$t0, screen
	addi	$t0, $t0, 3740
	
	addi	$t9, $zero, 7
	j	HeartLoop
HeartLoop:
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	subi	$t9, $t9, 1
	bne 	$zero, $t9, HeartLoop
	
	
	lw 	$t0, screen
	addi	$t0, $t0, 3996
	
	addi	$t9, $zero, 7
	j	HeartLoop1
HeartLoop1:
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	subi	$t9, $t9, 1
	bne 	$zero, $t9, HeartLoop1
		
			
	lw 	$t0, screen
	addi	$t0, $t0, 4256
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)			
	
	
	lw 	$t0, screen
	addi	$t0, $t0, 4516
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	lw 	$t0, screen
	addi	$t0, $t0, 4776
	sw	$t2, ($t0)
	j	SwordIcon
	
SwordIcon:
	li	$t2, 0x00808080
	lw 	$t0, screen
	addi	$t0, $t0, 5552
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
		
	lw 	$t0, screen
	addi	$t0, $t0, 5804
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)		
			
	lw 	$t0, screen
	addi	$t0, $t0, 6056
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	
	lw 	$t0, screen
	addi	$t0, $t0, 6312
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	
	li	$t2, 0x00A52A2A
	lw 	$t0, screen
	addi	$t0, $t0, 5796
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)	
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)	
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	
					
	lw 	$t0, screen
	addi	$t0, $t0, 6560
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	
	lw	$t0, screen
	addi	$t0, $t0, 6816
	sw	$t2, ($t0)
	j	Colon1
	
Colon1: 
	li	$t2, 0x00ffffff
	lw 	$t0, screen
	addi	$t0, $t0, 4028
	sw	$t2, ($t0)
	addi	$t0, $t0, 512
	sw	$t2, ($t0)
	addi	$t0, $t0, 1536
	sw	$t2, ($t0)
	addi	$t0, $t0, 512
	sw	$t2, ($t0)
	jr	$ra

###############################################################################################################################
Number0: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	li	$t2, 0x00000000
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	li	$t2, 0x00ffffff
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	li	$t2, 0x00000000
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	li	$t2, 0x00ffffff
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	li	$t2, 0x00000000
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	li	$t2, 0x00ffffff
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	lw	$t0, screen
	jr 	$ra
	
Number1: 
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr	$ra
	
Number2: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr	$ra	
	
Number3: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr	$ra	
	
	
Number4: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr	$ra
	
Number5: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	lw	$t0, screen
	jr	$ra
	
Number6: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	lw	$t0, screen
	jr	$ra		
	
Number7: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr 	$ra
	
	
Number8: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr	$ra	
	
Number9: 
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	li	$t2, 0x00000000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	li	$t2, 0x00ffffff
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)	
	lw	$t0, screen
	jr	$ra	
#################################################################################################################################
DrawEnemy:
	lw 	$t0, screen
        addi 	$t0, $t0, 2600
        lw 	$t4, roomsize
        j 	DrawEnemyFace1
DrawEnemyFace1:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace1 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2856
	j	DrawEnemyFace2
DrawEnemyFace2:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace2 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3112
	j	DrawEnemyFace3
DrawEnemyFace3:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace3 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3368
	j	DrawEnemyFace4
DrawEnemyFace4:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace4 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3624
	j	DrawEnemyFace5
DrawEnemyFace5:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace5 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3880
	j	DrawEnemyFace6
DrawEnemyFace6:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace6 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4136
	j	DrawEnemyFace7
DrawEnemyFace7:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace7 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4392
	j	DrawEnemyFace8
DrawEnemyFace8:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace8 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4648
	j	DrawEnemyFace9
DrawEnemyFace9:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace9 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4904
	j	DrawEnemyFace10
DrawEnemyFace10:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawEnemyFace10 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 5164
	j	DrawEnemyFace11
DrawEnemyFace11:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 8, DrawEnemyFace11 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4400
	j	DrawEnemyLoop5
	
	
#mouth 4400
DrawEnemyLoop5:
	li	$t2, 0x006600	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 12, DrawEnemyLoop5 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3120
	j	DrawEnemyLoop6
DrawEnemyLoop6:
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3148
	j	DrawEnemyLoop7
DrawEnemyLoop7:
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2372
	j	DrawEnemyLoop8
#horns
DrawEnemyLoop8:    	
	li    $t2, 0xcccccc
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 6
	bgt	$t4, 0, DrawEnemyLoop8
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2120
    	j    DrawEnemyLoop9

DrawEnemyLoop9:
    	li    $t2, 0x737373
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 8
	bgt	$t4, 4, DrawEnemyLoop9
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 1868
	j	DrawEnemyLoop10
DrawEnemyLoop10:
	li	$t2, 0xcccccc	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2344
	j	DrawEnemyLoop11
DrawEnemyLoop11:    	
	li    $t2, 0xcccccc
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 6
	bgt	$t4, 0, DrawEnemyLoop11
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2092
    	j    DrawEnemyLoop12

DrawEnemyLoop12:
    	li    $t2, 0x737373
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 8
	bgt	$t4, 4, DrawEnemyLoop12
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 1840
	j	DrawEnemyLoop13
DrawEnemyLoop13:
	li	$t2, 0xcccccc	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 4660
	j	DrawEnemyLoop14
DrawEnemyLoop14:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 4680
	j	DrawEnemyLoop15
DrawEnemyLoop15:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	j 	conditions
##################################################################################################################################################
DrawFirstBoss:
	lw 	$t0, screen
    	addi 	$t0, $t0, 2600
    	lw	$t4, roomsize
    	j DrawFirstBossFace1
DrawFirstBossFace1:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace1 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2856
	j	DrawFirstBossFace2
DrawFirstBossFace2:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace2 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3112
	j	DrawFirstBossFace3
DrawFirstBossFace3:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace3 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3368
	j	DrawFirstBossFace4
DrawFirstBossFace4:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace4 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3624
	j	DrawFirstBossFace5
DrawFirstBossFace5:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace5 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3880
	j	DrawFirstBossFace6
DrawFirstBossFace6:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace6 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4136
	j	DrawFirstBossFace7
DrawFirstBossFace7:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace7 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4392
	j	DrawFirstBossFace8
DrawFirstBossFace8:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace8 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4648
	j	DrawFirstBossFace9
DrawFirstBossFace9:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace9 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4904
	j	DrawFirstBossFace10
DrawFirstBossFace10:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFirstBossFace10 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 5164
	j	DrawFirstBossFace11
DrawFirstBossFace11:
	li	$t2, 0x801a00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 8, DrawFirstBossFace11 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4400
	j	DrawFirstBossLoop5
	
	
#mouth 4400
DrawFirstBossLoop5:
	li	$t2, 0x006600	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 12, DrawFirstBossLoop5 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3120
	j	DrawFirstBossLoop6
DrawFirstBossLoop6:
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3148
	j	DrawFirstBossLoop7
DrawFirstBossLoop7:
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2372
	j	DrawFirstBossLoop8
#horns
DrawFirstBossLoop8:    	
	li    $t2, 0xb38600
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 6
	bgt	$t4, 0, DrawFirstBossLoop8
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2120
    	j    DrawFirstBossLoop9

DrawFirstBossLoop9:
    	li    $t2, 0x806000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 8
	bgt	$t4, 4, DrawFirstBossLoop9
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 1868
	j	DrawFirstBossLoop10
DrawFirstBossLoop10:
	li	$t2, 0x4d3900	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2344
	j	DrawFirstBossLoop11
DrawFirstBossLoop11:    	
	li    $t2, 0xb38600
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 6
	bgt	$t4, 0, DrawFirstBossLoop11
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2092
    	j    DrawFirstBossLoop12

DrawFirstBossLoop12:
    	li    $t2, 0x806000
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 8
	bgt	$t4, 4, DrawFirstBossLoop12
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 1840
	j	DrawFirstBossLoop13
DrawFirstBossLoop13:
	li	$t2, 0x4d3900	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 4660
	j	DrawFirstBossLoop14
DrawFirstBossLoop14:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 4680
	j	DrawFirstBossLoop15
DrawFirstBossLoop15:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2608
	j	DrawFirstBossEyebrows
DrawFirstBossEyebrows:
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2612
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2868
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2872
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3128
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3140
	
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2884
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2888
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2632
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2636
	li	$t2, 0x664400	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	j	conditions

#######################################################################################################################
DrawFinalBoss:
	lw $t0, screen
    	addi $t0, $t0, 2600
    	lw	$t4, roomsize
    	j DrawFinalBossFace1

DrawFinalBossFace1:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace1 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2856
	j	DrawFinalBossFace2
DrawFinalBossFace2:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace2 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3112
	j	DrawFinalBossFace3
DrawFinalBossFace3:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace3 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3368
	j	DrawFinalBossFace4
DrawFinalBossFace4:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace4 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3624
	j	DrawFinalBossFace5
DrawFinalBossFace5:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace5 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3880
	j	DrawFinalBossFace6
DrawFinalBossFace6:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace6 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4136
	j	DrawFinalBossFace7
DrawFinalBossFace7:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace7 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4392
	j	DrawFinalBossFace8
DrawFinalBossFace8:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace8 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4648
	j	DrawFinalBossFace9
DrawFinalBossFace9:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace9 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4904
	j	DrawFinalBossFace10
DrawFinalBossFace10:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossFace10 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 5164
	j	DrawFinalBossFace11
DrawFinalBossFace11:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 8, DrawFinalBossFace11 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 5424
	j	DrawFinalBossFace12
DrawFinalBossFace12:
	li	$t2, 0x248f24	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 12, DrawFinalBossFace12 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4396
	j	DrawFinalBossLoop5
	
	
#######################################################################################################
	
DrawFinalBossLoop5:
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 8, DrawFinalBossLoop5 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4656
	j	DrawFinalBossTeeth1
DrawFinalBossTeeth1:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 22, DrawFinalBossTeeth1 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4676
	j	DrawFinalBossTeeth2
DrawFinalBossTeeth2:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 22, DrawFinalBossTeeth2 #loops until wall is complete
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4916
	j	DrawFinalBossTeeth3
DrawFinalBossTeeth3:
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 4936
	li	$t2, 0xffffff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3120
	j	DrawFinalBossLoop6
DrawFinalBossLoop6:
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3148
	j	DrawFinalBossLoop7
DrawFinalBossLoop7:
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2344
	j	DrawFinalBossLoop8
DrawFinalBossLoop8:    	
	li    $t2, 0xffff00
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossLoop8
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2088
    	j    DrawFinalBossLoop9

DrawFinalBossLoop9:
    	li    $t2, 0xffff00
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossLoop9
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 1832
	j	DrawFinalBossLoop10
DrawFinalBossLoop10:
	li	$t2, 0xffff00	#sets color
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sub	$t4, $t4, 2
	bgt	$t4, 4, DrawFinalBossLoop10
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 1576
	j	DrawFinalBossCrown
#Draws The Boss Crown
DrawFinalBossCrown:
	li	$t2, 0xffff00	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 1580
	li	$t2, 0x000000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 1588
	li	$t2, 0xffff00	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 1592
	li	$t2, 0xffff00	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 1604
	li	$t2, 0xffff00	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 1608
	li	$t2, 0xffff00	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 1620
	li	$t2, 0xffff00
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2092
	#adding gems to the crown
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2096
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2108
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2112
	li	$t2, 0xaa00ff	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2124
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2128
	li	$t2, 0xff0000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2608
	j	BossLoop12
BossLoop12:
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2868
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 3128
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2612
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2872
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 3140
#########right eyebrow########################
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2884
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2888
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi 	$t0, $t0, 2632
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 2636
	li	$t2, 0x1a6600	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3908
	li	$t2, 0x000000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	addi	$t0, $t0, 3896
	li	$t2, 0x000000	#sets color
	sw	$t2, ($t0)
	lw	$t4, roomsize
	lw	$t0, screen
	j	conditions
###########################################################################################################
Menu: 	
	li	$t2, 0x00800080

	lw	$t0, screen
	addi	$t0, $t0, 264
	jal	LetterT
	lw	$t0, screen
	addi	$t0, $t0, 280
	jal	LetterE
	lw	$t0, screen
	addi	$t0, $t0, 300
	jal	LetterX
	lw	$t0, screen
	addi	$t0, $t0, 320
	jal	LetterT
	lw	$t0, screen
	addi	$t0, $t0, 344
	jal	LetterS
	lw	$t0, screen
	addi	$t0, $t0, 364
	jal	LetterQ
	lw	$t0, screen
	addi	$t0, $t0, 388
	jal	LetterU
	lw	$t0, screen
	addi	$t0, $t0, 408
	jal	LetterA
	lw	$t0, screen
	addi	$t0, $t0, 428
	jal	LetterB
	lw	$t0, screen
	addi	$t0, $t0, 448
	jal	LetterB
	lw	$t0, screen
	addi	$t0, $t0, 468
	jal	LetterL
	lw	$t0, screen
	addi	$t0, $t0, 488
	jal	LetterE
	lw	$t0, screen
	addi	$t0, $t0, 1804
	jal	LetterW
	lw	$t0, screen
	addi	$t0, $t0, 1828
	jal	LetterI
	lw	$t0, screen
	addi	$t0, $t0, 1836
	jal	LetterT
	lw	$t0, screen
	addi	$t0, $t0, 1852 
	jal	LetterH
	lw	$t0, screen
	addi	$t0, $t0, 1880
	jal	LetterM
	lw	$t0, screen
	addi	$t0, $t0, 1904
	jal	LetterO
	lw	$t0, screen
	addi	$t0, $t0, 1924
	jal	LetterN
	lw	$t0, screen
	addi	$t0, $t0, 1944
	jal	LetterS
	lw	$t0, screen
	addi	$t0, $t0, 1964
	jal	LetterT
	lw	$t0, screen
	addi	$t0, $t0, 1980
	jal	LetterE
	lw	$t0, screen
	addi	$t0, $t0, 2000
	jal	LetterR
	lw	$t0, screen
	addi	$t0, $t0, 2020
	jal	LetterS
	lw	$t0, screen
	addi	$t0, $t0, 4440
	jal	LetterP
	lw	$t0, screen
	addi	$t0, $t0, 4460
	jal	LetterL
	lw	$t0, screen
	addi	$t0, $t0, 4480
	jal	LetterA
	lw	$t0, screen
	addi	$t0, $t0, 4500
	jal	LetterY
	j	MenuLoop

GameOverText:
	li	$t2, 0x00FF0000

	lw	$t0, screen
	addi	$t0, $t0, 3368
	jal	LetterG
	lw	$t0, screen
	addi	$t0, $t0, 3388
	jal	LetterA
	lw	$t0, screen
	addi	$t0, $t0, 3408
	jal	LetterM
	lw	$t0, screen
	addi	$t0, $t0, 3432
	jal	LetterE
	lw	$t0, screen
	addi	$t0, $t0, 3464
	jal	LetterO
	lw	$t0, screen
	addi	$t0, $t0, 3484
	jal	LetterV
	lw	$t0, screen
	addi	$t0, $t0, 3504
	jal	LetterE
	lw	$t0, screen
	addi	$t0, $t0, 3524
	jal	LetterR
	j	StopProgram
Congrats:
	li	$t2, 0x00008000

	lw	$t0, screen
	addi	$t0, $t0, 3380
	jal	LetterC
	lw	$t0, screen
	addi	$t0, $t0, 3400
	jal	LetterO
	lw	$t0, screen
	addi	$t0, $t0, 3420
	jal	LetterN
	lw	$t0, screen
	addi	$t0, $t0, 3440
	jal	LetterG
	lw	$t0, screen
	addi	$t0, $t0, 3460
	jal	LetterR
	lw	$t0, screen
	addi	$t0, $t0, 3480
	jal	LetterA
	lw	$t0, screen
	addi	$t0, $t0, 3500
	jal	LetterT
	lw	$t0, screen
	addi	$t0, $t0, 3516
	jal	LetterS
	
	j	StopProgram
Retry:
	li 	$t2, 0x00808080
	lw	$t0, screen
	addi	$t0, $t0, 2628
	jal	LetterR
	lw	$t0, screen
	addi	$t0, $t0, 2648
	jal	LetterE
	lw	$t0, screen
	addi	$t0, $t0, 2668
	jal	LetterT
	lw	$t0, screen
	addi	$t0, $t0, 2684
	jal	LetterR
	lw	$t0, screen
	addi	$t0, $t0, 2704
	jal	LetterY
	lw	$t0, screen
	addi	$t0, $t0, 2728
	jal	QuestionMark
	li	$t2, 0x00008000
	lw	$t0, screen
	addi	$t0, $t0, 4188
	jal	LetterY
	li 	$t2, 0x00808080
	lw	$t0, screen
	addi	$t0, $t0, 4212
	jal	Slash
	li	$t2, 0x00FF0000
	lw	$t0, screen
	addi	$t0, $t0, 4236
	jal	LetterN2
	
	j 	RetryLoop
	

LetterT: 
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 252
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterE:
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 260
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterX:
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)	
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterS:	
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterQ:
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterU:
	sw	$t2, ($t0)	
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)		
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterA:
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)	
	
	jr 	$ra
	
LetterB:
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterL:
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterW:
	sw	$t2, ($t0)
	addi	$t0, $t0, 16
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 16
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)	
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterI:
	sw	$t2, ($t0)	
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	addi	$t0, $t0, 256
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterH:
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	addi	$t0, $t0, 244
	sw	$t2, ($t0)
	addi	$t0, $t0, 12
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterM:
	sw	$t2, ($t0)	
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 240
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	addi	$t0, $t0, 8
	sw	$t2, ($t0)
	
	jr 	$ra

LetterO:	
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterN:					
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterR:					
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 8						
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterP:					
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterY:							
	sw	$t2, ($t0)
	addi	$t0, $t0, 16						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 8						
	sw	$t2, ($t0)
	addi	$t0, $t0, 252						
	sw	$t2, ($t0)
	addi	$t0, $t0, 256						
	sw	$t2, ($t0)
	addi	$t0, $t0, 256						
	sw	$t2, ($t0)
	
	jr 	$ra	
	
LetterG:						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 256						
	sw	$t2, ($t0)
	addi	$t0, $t0, 8						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	
	jr 	$ra
	
LetterV:					
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	
	jr 	$ra
				
LetterC:
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 244						
	sw	$t2, ($t0)
	addi	$t0, $t0, 256						
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	
	jr 	$ra	
	
QuestionMark:					
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4					
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 256					
	sw	$t2, ($t0)
	addi	$t0, $t0, 248						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 504						
	sw	$t2, ($t0)
	
	jr 	$ra		

Slash:					
	addi	$t0, $t0, 16						
	sw	$t2, ($t0)
	addi	$t0, $t0, 252						
	sw	$t2, ($t0)
	addi	$t0, $t0, 252					
	sw	$t2, ($t0)
	addi	$t0, $t0, 252						
	sw	$t2, ($t0)
	addi	$t0, $t0, 252					
	sw	$t2, ($t0)
	
	jr 	$ra																																																																																																			
		
LetterN2:
	sw	$t2, ($t0)
	addi	$t0, $t0, 16						
	sw	$t2, ($t0)
	addi	$t0, $t0, 240						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4					
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 240					
	sw	$t2, ($t0)
	addi	$t0, $t0, 8					
	sw	$t2, ($t0)
	addi	$t0, $t0, 8					
	sw	$t2, ($t0)
	addi	$t0, $t0, 240					
	sw	$t2, ($t0)
	addi	$t0, $t0, 12						
	sw	$t2, ($t0)
	addi	$t0, $t0, 4						
	sw	$t2, ($t0)
	addi	$t0, $t0, 240						
	sw	$t2, ($t0)
	addi	$t0, $t0, 16						
	sw	$t2, ($t0)
	
	jr 	$ra	
################Print To Display functions################
printString:	
	stringSetup:	
		sw	$ra,outputRegisterIndicator #Ra is part of how we loop the program; we put calls back to ra in future methods.
		ori	$t4, $a0, 0 #Move string from $a0 to $s0
		ori	$t2, $0, 0 #Start a count for # of characters in string.
	stringLoop:	
		lb  	$a0, 0($t4) #Pull one character off our stored string
		beq 	$a0,$0,endOfString #Check if we have reached end of string, if we have go to a method called endOfString which wraps things up.
		jal	loadOutputRegister #Load the output register
		addi	$t2, $t2, 1 #increment our inputRegister and inputRegisterIndicator
		addi	$t4, $t4, 1 #to "move" to the next character in our string
		j	stringLoop  #Loop back and do it all again, will print the other character we pulled in the meantime.
	endOfString:	
		ori	$v0, $t2, 0 #Set the character counter to 0 so we can print other strings later   
		lw	$ra,outputRegisterIndicator #Reset $ra so we can print another string later.
	loadOutputRegister:		
		lui     $t0,0xffff #Loads t0 with 0xffff, then we can just add on the number of the register we need. E.g c for output, 4 for input etc.		
	printFromOutputRegister:
		lw	$t1,8($t0)#Set t1 to the outputRegisterIndicator		
		beq	$t1,$0,printFromOutputRegister #Check if that register is 0, if it is, loop back, in other words, wait until we are ready to send the character.
		sw	$a0, 0xc($t0) #Print the (first) character in $a0 to the display. 
		jr	$ra #Return all the way back to stringSetup.

GameOver: #terminate program
	li	$t2, 0x00000000
	lw	$t0, screen
	li	$t7, 100
	j	GameOverLoop

GameOverLoop:
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	ble	$t0, 0x10008000, GameOverLoop
	j	Retry

RetryLoop:
	beq	$t6, 6, Initilize
	beq	$t6, 7, Exit
	j	conditions
	
GameComplete:
	jal	Wipe
	j	Congrats

Exit:
	li	$t2, 0x00000000
	lw	$t0, screen
	j	ExitLoop
ExitLoop:
	sw	$t2, ($t0)
	addi	$t0, $t0, 4
	ble	$t0, 0x10008000, ExitLoop
	j	GameOverText	#Displays "Game Over" on the screen and ends the program
StopProgram:	
	li	$v0, 10
	syscall
