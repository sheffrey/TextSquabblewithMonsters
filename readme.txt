==========================================TSM PROJECT - TEXT SQUABBLE WITH MONSTERS - README==========================================
Dale Fu - daf055
Mitchell Gaertner - mjg539
Ryan Hoppe - rmh898
Shefik Mujkic - shm413
Auston Tran - amt838
======================================================================================================================================
Current Version: TSMGameV3.1.asm


To launch the program, first open up the modified version of Mars MIPS jar file that is included.

-Select File > Open and open the TSMGameV3.1.asm program file.

-Select Tools > Bitmap Display.
    *Set both Unit Width in Pixels, and Unit Height in Pixels to 8.
    *Set the Display Width in Pixels to 512, and the Display Height in Pixels to 256.
    *Set Base address for display to 0x10000000 (global data).
    -Hit the Connect to MIPS button
    
-Select Tools > Keyboard and Display MMIO Simulator.
    -Hit the Connect to MIPS button.

-Select Run > Assemble to compile and ready our program to be run.

-Select Run > Go to execute the program.

-Select the Reset button on the Keyboard and Display MMIO Simulator after you have compiled and started the game.

-Enter a space character in the Keyboard and Display MMIO Simulator box to start the game.

-The user will be prompted in the message log on instructions on how to traverse through the map.

Good Luck! 